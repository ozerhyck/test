
--Creating a Database for this example.
CREATE DATABASE DB_Finantial_Test;
GO

--Adjusting the script for this new database.
USE DB_Finantial_Test;
GO

--Creating a User Defined Type (UDT) for the input list.
CREATE TYPE dbo.FinancialInstrument_UDT AS TABLE
(
    MarketValue FLOAT,
    Type NVARCHAR(15)
);
GO

--Creating a table to handle the Financial Instruments.
CREATE TABLE dbo.FinancialInstruments
(
    ID INT IDENTITY(1,1) PRIMARY KEY,
    MarketValue FLOAT,
    Type NVARCHAR(100)
);
GO

--Creating a function to categorize the values.
CREATE FUNCTION dbo.fncClassifyMarketValue 
(
    @MarketValue FLOAT
)
RETURNS NVARCHAR(15)
AS
BEGIN
    DECLARE @Category NVARCHAR(15);

    IF @MarketValue < 1000000
        SET @Category = 'Low Value'
    ELSE IF @MarketValue BETWEEN 1000000 AND 5000000
        SET @Category = 'Medium Value'
    ELSE
        SET @Category = 'High Value'

    RETURN @Category;
END;
GO

--Main procedure, the final object of the test.
CREATE PROCEDURE [dbo].[FinantialInstrumentCategories] (
    @Instruments as dbo.FinancialInstrument_UDT READONLY
)
AS
BEGIN
    --Clear data to reuse, just to example
    TRUNCATE TABLE FinancialInstruments;

    --Inserting new data to obtain an ID beacause we created it as IDENTITY
    INSERT INTO dbo.FinancialInstruments (MarketValue, [Type])
    SELECT 
        MarketValue, 
        [Type] 
    FROM 
        @Instruments;

    --Aligning and separating by comma.
    SELECT DISTINCT
    STUFF(
            (
                SELECT 
                    ', ' + dbo.fncClassifyMarketValue(t2.MarketValue)
                FROM 
                    FinancialInstruments t2
                WHERE 
                    1=1
                FOR XML PATH ('')
            )
            , 1, 1, ''
        )  AS InstrumentCategories
    FROM 
        FinancialInstruments t1;

END;
GO

--Testing! Declare an UDT
DECLARE @MyInstruments dbo.FinancialInstrument_UDT;

-- Inserting values into the list (our UDT)
INSERT INTO @MyInstruments (MarketValue, Type) VALUES (800000, 'Stock');
INSERT INTO @MyInstruments (MarketValue, Type) VALUES (1500000, 'Bond');
INSERT INTO @MyInstruments (MarketValue, Type) VALUES (6000000, 'Derivative');
INSERT INTO @MyInstruments (MarketValue, Type) VALUES (300000, 'Stock');

--And passing it as a parameter to obtain the desired output
EXEC FinantialInstrumentCategories @Instruments = @MyInstruments
