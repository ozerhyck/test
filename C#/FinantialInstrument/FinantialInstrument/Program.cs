﻿using FinantialInstruments.Categories;
using FinantialInstruments.Models;

//Considerations:
//This exercise involves several object-oriented programming features, such as:
//Abstraction - Category
//Inheritance - Interface
//Polymorphism - low/medium/high
//SOLID principles
//As for the Design Pattern, "Chain of Responsibility" was chosen - https://www.dofactory.com/net/chain-of-responsibility-design-pattern
//This is a classic case of the chain of responsibility, as the request is to categorize with well-defined rules that follow a sequence and cannot be two things at the same time.
//The chain of responsibility adheres very well to the SOLID principles.
//It is Closed for modification, Open for extensions -- Open Closed Principle (OCP).
//So, in the case of adding, removing, or making future adjustments and modifications, this will be quite simple.
//For example: Since each rule has its own class, for new rules, it is enough to include new classes.

//Instantiates the categories
Category low = new LowValue();
Category medium = new MediumValue();
Category high = new HighValue();

//Defines the successors
low.SetSuccessor(medium);
medium.SetSuccessor(high);

//Input list
List<FinantialInstrument> FinantialInstruments = new()
{
    new() { MarketValue = 800000, Type = "Stock"},
    new() { MarketValue = 1500000, Type = "Bond" },
    new() { MarketValue = 6000000, Type = "Derivative"},
    new() { MarketValue = 300000, Type = "Stock" }
};

//Output List
List<string> instrumentCategories = new();

//Goes through the list to apply the rules to each item efficiently
FinantialInstruments.ForEach(fi => low.HandleRequest(fi, ref instrumentCategories));

//Prints the output
Console.WriteLine(string.Concat("instrumentCategories = {'", string.Join("', '", instrumentCategories), "'}"));
Console.ReadKey();