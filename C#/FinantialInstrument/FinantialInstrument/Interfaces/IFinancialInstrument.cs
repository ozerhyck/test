﻿namespace FinantialInstruments.Interfaces
{
    /// <summary>
    /// The FinancialInstrument Interface implements:
    /// MarketValue (double), Type (string).
    /// </summary>
	public interface IFinancialInstrument
	{
        double MarketValue { get; }
        string Type { get; }
    }
}

