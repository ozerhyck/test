﻿using FinantialInstruments.Interfaces;

namespace FinantialInstruments.Categories
{
    public class LowValue : Category
	{
        //If there is a need to modify anything related to low Value, it will be done only here.
        public override void HandleRequest(IFinancialInstrument instrument, ref List<string> instrumentCategories)
        {
            if (instrument.MarketValue < 1000000)
            {
                instrumentCategories.Add("Low Value");
            }
            else
            {
                Successor?.HandleRequest(instrument, ref instrumentCategories);
            }
        }
    }
}

