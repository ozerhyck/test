﻿using FinantialInstruments.Interfaces;

namespace FinantialInstruments.Categories
{
    public class MediumValue : Category
    {
        //If there is a need to modify anything related to Medium Value, it will be done only here.
        public override void HandleRequest(IFinancialInstrument instrument, ref List<string> instrumentCategories)
        {
            if (instrument.MarketValue >= 1000000 && instrument.MarketValue <= 5000000) {
                instrumentCategories.Add("Medium Value");
            }
            else
            {
                Successor?.HandleRequest(instrument, ref instrumentCategories);
            }
        }
    }
}

