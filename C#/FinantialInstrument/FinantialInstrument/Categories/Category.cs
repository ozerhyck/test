﻿using FinantialInstruments.Interfaces;

namespace FinantialInstruments.Categories
{
    public abstract class Category
	{
        //To succeed in the validation.
        protected Category? Successor;

		public void SetSuccessor(Category successor)
		{
			Successor = successor;
        }

        //Handle with the request, testing the Financial Instrument, and passes the reference of the output list. 
        public abstract void HandleRequest(IFinancialInstrument instrument, ref List<string> instrumentCategories);
	}
}

