﻿using FinantialInstruments.Interfaces;

namespace FinantialInstruments.Categories
{
    public class HighValue : Category
    {
        //If there is a need to modify anything related to High Value, it will be done only here.
        public override void HandleRequest(IFinancialInstrument instrument, ref List<string> instrumentCategories)
        {
            if (instrument.MarketValue > 5000000) {
                instrumentCategories.Add("High Value");
            }
            else
            {
                Successor?.HandleRequest(instrument,ref instrumentCategories);
            }
        }
    }
}

