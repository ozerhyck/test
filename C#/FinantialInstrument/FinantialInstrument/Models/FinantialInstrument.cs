﻿using FinantialInstruments.Interfaces;

namespace FinantialInstruments.Models
{
    public class FinantialInstrument : IFinancialInstrument
    {
        public required double MarketValue { get; set; }
        public required string Type { get; set; }
    }
}

